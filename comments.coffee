class Comment

	constructor: (data)->

		ko.mapping.fromJS data, {}, @

		@text = ko.observable data?.text
		@likes = ko.observable data?.likes ? 0

		default_state = editable: ko.observable(yes), liked: ko.observable(no)
		@state = ko.mapping.fromJS data?.state ? default_state

		@is_editing = ko.observable no

		# subscribe to events for real-time updates
		@text.subscribe (new_text)-> console.debug 'Comment text changed to', new_text if new_text.length > 0

		@state.liked.subscribe (liked)->
			verb = if liked then 'did' else 'does not'
			console.debug "User #{verb} like comment"

		@is_editing.subscribe (editing)->
			verb = if editing then 'entered' else 'exited'
			console.log "Comment #{verb} editing mode"

	@like: (comment)->
		comment.state.liked yes
		likes = parseInt comment.likes()
		comment.likes ++likes


	@unlike: (comment)->
		comment.state.liked no
		likes = parseInt comment.likes()
		comment.likes --likes


	@edit: (comment, e)-> comment.is_editing yes

	@update: (comment, event)->

		key_code = event?.which ? event.keyCode
		if key_code is 13
			updated_comment = $(event.currentTarget).val().trim()
			if updated_comment.length > 0
				comment.is_editing no
				comment.text updated_comment if updated_comment isnt comment.text()
				return false
		return true



class Discussion

	@mapping: create: (comment)-> new Comment comment.data

	constructor: (comments)->

		@comments = ko.mapping.fromJS []
		ko.mapping.fromJS comments, Discussion.mapping, @comments

		@comments_count = ko.computed => @comments().length

		@add_comment = (discussion)=>
			unless $('#new-comment-text').val().length > 0
				return alert 'Enter comment first'

			@comments.unshift new Comment
				text: $('#new-comment-text').val()
				user: name: 'Martaban Asal'
				created_at: new Date
			$('#new-comment-text').val ''

		@remove_comment = (comment)=> @comments.remove comment

		@comment_template = (comment)=> console.log comment

		@new_comment_keypress = (discussion, event)->
			key_code = event?.which ? event.keyCode
			if key_code is 13
				new_comment = $(event.currentTarget).val().trim()
				if new_comment.length > 0
					@add_comment new_comment
					return false
			return true

		@focus_edit_comment_field = (element, comment)->
			$(element).find('.edit-comment-text').focus()
			$('.edit-comment-text').autosize()

		# proxy comment actions
		@like_comment   = Comment.like
		@unlike_comment = Comment.unlike
		@edit_comment   = Comment.edit
		@update_comment = Comment.update

	@make = (comments)-> ko.applyBindings new Discussion comments

$ ->

	jqxhr = $.ajax
				url :'/data/comments.json'
				dataType: 'json'
				success: Discussion.make

	$('#new-comment-text').autosize()